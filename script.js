const userUrl = 'https://ajax.test-danit.com/api/swapi/films';
const container = document.querySelector('.list');

fetch(userUrl)
  .then((data) => data.json())
  .then((films) => {
    const filmList = document.createElement('ul');
    filmList.classList.add('film__list');

    const fetches = films.map((film) => {
      return fetchFilmData(film);
    });

    Promise.all(fetches)
      .then((filmsWithData) => {
        filmsWithData.sort((first, second) => first.episodeId - second.episodeId);

        filmsWithData.forEach((film) => {
          const filmItem = filmTemplate(film);
          filmList.append(filmItem);
        });

        container.append(filmList);
      });
  });

function fetchFilmData(film) {
  return fetch(film.url)
    .then((data) => data.json());
}

function filmTemplate(film) {
  const filmItem = document.createElement('li');
  const subFilmUl = document.createElement('ol');

  film.characters.forEach((item) => {
    fetch(item)
      .then((data) => data.json())
      .then(person => {
        const subFilmLi = document.createElement('li');
        subFilmLi.innerText = person.name;
        subFilmUl.append(subFilmLi);
      })
  })

  filmItem.innerHTML = `<h3>Film ID: ${film.episodeId}, Name: ${film.name}, Opening quote:</h3>${film.openingCrawl}<h3>Characters:</h3>`;

  subFilmUl.classList.add('characters__data');
  filmItem.classList.add('film__data');

  filmItem.append(subFilmUl);

  return filmItem;
}